reset

# ZADANIE 1
file_ex_1_heatflux = "1.dat"         # plik zawierający 4 kolumny:   1: t      2: qRt)  3: qB(t)  4: (qR+qB)(t)
file_ex_1_tempdist = "1_1.map"       # plik zawierający 3 kolumny:   1: x      2: y     3: T(x,y,t_end)

# ZADANIE 2
file_ex_2_heatflux = "2.dat"         # plik zawierający 4 kolumny:   1: t      2: qRt)  3: qB(t)  4: (qR+qB)(t)
format_ex_2_tempdist = "2_%1d.map"   # pliki zawierające 3 kolumny:  1: x      2: y     3: T(x,y,t)

plot_ex_2_heatflux = 1               # 1 by wyrysować strumień ciepła dla drugiego zadania

################################

set term pngcairo size 1000,600 enhanced

if (GPVAL_VERSION >= 5.0) set for [i=1:9] linetype i dashtype i
if (GPVAL_VERSION < 5.0)  set termoption dashed

################################

# Zadanie 1
# Strumień ciepła

set xlabel "t"
set ylabel "q"

set autoscale xy

set logscale y

set out "ex_1_q.png"
p file_ex_1_heatflux u 1:2 w l lt 1 lw 3 lc rgb "red"  t "q_R", \
                  "" u 1:3 w l lt 2 lw 3 lc rgb "blue" t "q_B", \
                  "" u 1:4 w l lt 1 lw 3 lc rgb "gray" t "q_R + q_B"

# Zadanie 1
# Rozkład temperatury

set xlabel "x"
set ylabel "y"
set cblabel "T"

set xrange [0:100]
set yrange [0:50]

unset logscale y
set size ratio -1

set out "ex_1_T.png"
p file_ex_1_tempdist u 1:2:3 w image notitle

################################

# Zadanie 2
# Strumień ciepła

if (plot_ex_2_heatflux == 1) {
  set xlabel "t"
  set ylabel "q"
  
  set autoscale xy
  
  set logscale y
  set size noratio
  
  set out "ex_2_q.png"
  p file_ex_2_heatflux u 1:2 w l lt 1 lw 3 lc rgb "red"  t "q_R", \
                    "" u 1:3 w l lt 2 lw 3 lc rgb "blue" t "q_B", \
                    "" u 1:4 w l lt 1 lw 3 lc rgb "gray" t "q_R + q_B"
}

# Zadanie 2
# Rozkłady temperatury

set xlabel "x"
set ylabel "y"
set cblabel "T"

set xrange [0:100]
set yrange [0:50]

unset logscale y
set size ratio -1

do for [frame = 1:5] {
  set output sprintf("ex_2_T_%1d.png", frame)
  set title sprintf("%1d", frame)
  
  filename = sprintf(format_ex_2_tempdist, frame)
  p filename u 1:2:3 w image notitle
}

set out