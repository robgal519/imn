#ifndef __lab6__
#define __lab6__

#include <vector>



typedef std::vector<std::vector<double> > Vector2D;

const int i_min = 0, i_1 = 20, i_2 = 40, i_3 = 60, i_4 = 80, i_max = 100;
const int j_min =0, j_1 =20, j_2 = 30, j_max = 50;
struct Config{
  double k;
  double rho;
  double C;
  double dx;
  double dy;
  double h;
  double t_0;
  double t_1;
  double t_2;
  double deltaT;
};

class Lab6{
  Config cfg;
  Vector2D T_prev;
  Vector2D T;
  double part1();
  double part2a(int x, int y);
  double part2(int x, int y);
  void prepareTables();
  void iteration();
  void conditions();
  double singleCondition(double point, double dXY);
  double integral();
  double CN();

public :
  Lab6(Config cfg):cfg(cfg){prepareTables();};

  void zad(int p);


};

#endif // __lab6__
