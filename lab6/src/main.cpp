#include "../lib/lab6.hpp"

int main()
{
  Config cfg;
  cfg.k=1;
  cfg.rho =1;
  cfg.C = 1;
  cfg.dx = cfg.dy = 1;
  cfg.h =0;
  cfg.t_0 = 0;
  cfg.t_1 = cfg.t_2 = 21;
  cfg.deltaT = 2.5;

  Lab6 lab(cfg);
  lab.zad(1);
  cfg.h =1e-2;
  Lab6 lab2(cfg);
   lab2.zad(2);

}
