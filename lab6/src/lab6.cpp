#include "../lib/lab6.hpp"
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
void Lab6::prepareTables(){
  T = Vector2D(101,std::vector<double>(51,0));

  for(int y=j_2;y<=j_max;y++)
    {
      T[i_min][y] = cfg.t_1;
      T[i_max][y] = cfg.t_2;
    }
  T_prev = T;
}

double Lab6::part1(){
  return (1.0/(1+(4*cfg.k*cfg.deltaT)/(2*cfg.rho*cfg.C*pow(cfg.dx,2))));
}
double Lab6::part2a(int x, int y){
  return  (T_prev[x][y-1] + T_prev[x][y+1] + T_prev[x-1][y]+T_prev[x-1][y]-4*T_prev[x][y]+
           T[x][y-1] + T[x][y+1] + T[x-1][y]+T[x-1][y]); 
    }
double Lab6::part2(int x, int y){
  return (T_prev[x][y]+((cfg.k*cfg.deltaT)/(2*cfg.rho*cfg.C*pow(cfg.dx,2)))*part2a(x,y));
          }

void Lab6::iteration(){
    for(int x=i_min;x<=i_max;x++)
      {
        for(int y=j_min;y<=j_max;y++)
          {
            if(x>=i_2 && x<=i_3 && y>=j_1)
              continue;
            if(x<=i_1 && y<=j_2)
              continue;
            if(x>i_4 && y<j_2)
              continue;
            if(x==i_min || x==i_max || y==j_min || y == j_max)
              continue;

            T[x][y] = part1()*part2(x,y);
          }
      }
   
  }
double Lab6::singleCondition(double point, double dXY)
{
  return (cfg.k/dXY * point + cfg.h*cfg.t_0)/(cfg.h + (cfg.k/dXY));
}

void Lab6::conditions(){
  //poziome krawędzie
  for(int x=0;x<=i_max;x++)
    {
      if(x>=i_2 && x<=i_3)
        T[x][j_1] = singleCondition(T[x][j_1-1], cfg.dy);
      else
        T[x][j_max] = singleCondition(T[x][j_max-1], cfg.dy);

      if(x>=i_1 && x<= i_4)
        T[x][j_min] = singleCondition(T[x][j_min+1],cfg.dy);
      else
        T[x][j_2] = singleCondition(T[x][j_2+1],cfg.dy);

    }
  //pionowe krawędzie
  for( int y=0; y<=j_max;y++)
    {
      if(y>=0 && y<=j_2)
        {
          T[i_1][y] = singleCondition(T[i_1+1][y], cfg.dx);
          T[i_4][y] = singleCondition(T[i_4-1][y], cfg.dx);
        }
      if(y>=j_1 && y<=j_max)
        {
          T[i_2][y] = singleCondition(T[i_2-1][y], cfg.dx);
          T[i_3][y] = singleCondition(T[i_3+1][y], cfg.dx);
        }
    }
  //narozniki
  T[i_2][j_max] = singleCondition(T[i_2-1][j_max-1], sqrt(2)*cfg.dx);
  T[i_2][j_1] = singleCondition(T[i_2-1][j_1-1], sqrt(2)*cfg.dx);

  T[i_3][j_max] = singleCondition(T[i_3+1][j_max-1], sqrt(2)*cfg.dx);
  T[i_3][j_1] = singleCondition(T[i_3+1][j_1-1], sqrt(2)*cfg.dx);

  T[i_4][j_2] = singleCondition(T[i_4-1][j_2+1], sqrt(2)*cfg.dx);
  T[i_4][j_min] = singleCondition(T[i_4-1][j_min+1], sqrt(2)*cfg.dx);

  T[i_1][j_2] = singleCondition(T[i_1+1][j_2+1], sqrt(2)*cfg.dx);
  T[i_1][j_min] = singleCondition(T[i_1+1][j_min+1], sqrt(2)*cfg.dx);

}

double Lab6::integral(){
  double sum = 0.0;
  for(int x=0;x<=i_max;x++)
    {
      for(int y=0;y<=j_max;y++)
        {
          sum += T[x][y];
        }
    }
  return sum;
}

double Lab6::CN(){
  double in=-100, prevIn=-1;
  while(fabs((prevIn-in)/prevIn )>1e-8)
    {
      conditions();
      iteration();
      prevIn = in;
      in = integral();
    }
  return in;
} 
void saveTable(Vector2D vec, std::string nazwa)
{
  std::fstream plik(nazwa,std::fstream::out);
  for(int x=0;x<=i_max;x++)
    {
      for(int y = 0; y<= j_max;y++)
        {
          plik <<x<<"\t"<<y<<"\t"<<vec[x][y]<<"\n";
        }
      plik<<"\n";
    }
}
void Lab6::zad(int p){
  double time = 0.0, outInt = - 1999, prevOutInt = -500;
  int i=1;
  // std::fstream calka(std::to_string(p)+".dat", std::fstream::out);
  for(;fabs((prevOutInt - outInt)/prevOutInt)>1e-8; time += cfg.deltaT)
    {
      prevOutInt = outInt;
      outInt = CN();
      T_prev = T;
      if(fabs(time - 125) < cfg.deltaT ||
         fabs(time- 250)<cfg.deltaT ||
         fabs(time - 500) <cfg.deltaT ||
         fabs(time - 100) <cfg.deltaT
         )
        saveTable(T,std::to_string(p)+"_"+std::to_string(i++)+".map");

    }

}
