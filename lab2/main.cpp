#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>


const int size=193;

typedef std::vector<std::vector<double> > array2D;

double rho(double x, double y)
{
	return 1/(pow(25,2)*M_PI)*exp(-pow(x/25,2)-pow(y/25,2));
}

/**
*edytuje tablicę array!
*/
void phi(int i, int j, double omega, array2D& array,double dx,int k)
{
array[i][j] = (1- omega)*array[i][j] + omega * (array[i+k][j] + array[i-k][j] + array[i][j+k] + array[i][j-k] + rho(-72+i*0.75, 75-j*0.75)*pow(k*dx,2))/4.0;
}

double integral(array2D array, double dx,int k)
{
double sum=0;
for(int i=0;i<size-k;i++)
{
	for(int j=0;j<size-k;j++)
	{
		sum += 0.5*(pow(array[i+k][j]-array[i][j],2) + pow(array[i][j+k]-array[i][j],2)) - k*k*pow(dx,2)*rho(-72+i*0.75, 72-j*0.75)*array[i][j];
	}
}
}

void zero(array2D& array)
{
	for(int i=0;i<array.size();i++)
	{
		for(int j=0;j<array[i].size();j++)
			array[i][j] = 0.0;
	}
}

void fillArrayPhi(array2D& array, double omega, double dx,int k)
{
	for(int i=k;i<size-k;i++)
		{
			for(int j=k;j<size-k;j++)
			{
				phi(i,j,omega,array,dx,k);
			}
		}
}

void zad1a(){
	array2D pudlo(size,std::vector<double>(size));
	std::fstream plik;
	plik.open(std::string("z1_")+".dat",std::fstream::out);
	double a = 0, a_prev = 0, epsilon=1e-8;
double omega = 1.99;
	fillArrayPhi(pudlo,omega,0.75,1);
	a = integral(pudlo,0.75,1);
	int iterator = 1;
	while(std::fabs(a-a_prev)>epsilon)
	{
		a_prev = a;
		fillArrayPhi(pudlo,omega,0.75,1);
		a = integral(pudlo,0.75,1);
		iterator ++;
	}

	for(int i=0;i<size;i++)
		{
			for(int j=0;j<size;j++)
			{
				plik<<-72+i*0.75<< "\t"<<72-j*0.75<<"\t"<<pudlo[i][j]<<std::endl;
			}
			plik<<std::endl;
		}
		plik.close();
}

void zad1()
{
	array2D pudlo(size,std::vector<double>(size));

	
	double a = 0, a_prev = 0, epsilon=1e-8;
	

	std::vector<double> omegaArray {0.6,0.8,1.0,1.5,1.9,1.95,1.99};
for(auto& omega: omegaArray){
	std::fstream plik;
	
	plik.open(std::string("z1_")+std::to_string(omega)+".dat",std::fstream::out);
	a = 0, a_prev = 0;
	zero(pudlo);
	fillArrayPhi(pudlo,omega,0.75,1);
	a_prev = integral(pudlo,0.75,1);

	fillArrayPhi(pudlo,omega,0.75,1);
	a = integral(pudlo,0.75,1);
	int iterator = 1;
	while(std::fabs(a-a_prev)>epsilon)
	{
		a_prev = a;
		fillArrayPhi(pudlo,omega,0.75,1);
		a = integral(pudlo,0.75,1);
		plik << iterator<< "\t"<<a<<"\n";
		iterator ++;
	}
plik.close();
}

}

double avr(double a, double b)
{return (a+b)/2.0;}

void zad2()
{
	std::fstream plik, map;
	double omega = 1;
double epsilon=1e-8;
 
	array2D pudlo(size,std::vector<double>(size));
	zero(pudlo);
	double a = 0, a_prev = 0;
	int iterator = 0;
for(int k=32;k>0;k/=2)
{
	plik.open(std::string("k")+std::to_string(k)+".dat",std::fstream::out);
	map.open(std::string("k")+std::to_string(k)+".map",std::fstream::out);
	fillArrayPhi(pudlo,omega,0.75,k);
	a_prev = integral(pudlo,0.75,k);

	fillArrayPhi(pudlo,omega,0.75,k);
	a = integral(pudlo,0.75,k);
	iterator ++;
	while(std::fabs(a-a_prev)>epsilon)
	{
		a_prev = a;
		fillArrayPhi(pudlo,omega,0.75,k);
		a = integral(pudlo,0.75,k);
		plik << iterator<< "\t"<<a<<"\n";
		iterator ++;
	}


	for(int i=k/2;i<size-k;i+=k)
	{
		for(int j=k/2;j<size-k;j+=k)
		{
			if(pudlo[i][j]!= 0)
			{
				if((i%k ==0 && j%k == 0) || (pudlo[i][j]!=0))
					break;
				if(i%k == 0)
				{
					pudlo[i][j]=avr(pudlo[i-k/2][j], pudlo[i+k/2][j]);
					break;
				}
				if(j%k ==0)
				{
					pudlo[i][j]=avr(pudlo[i][j-k/2], pudlo[i][j+k/2]);
					break;
				}
				pudlo[i][j] = (pudlo[i][j-k/2] + pudlo[i][j+k/2] + pudlo[i-k/2][j] + pudlo[i+k/2][j])/4.0;
			}

		}
	}

	for(int i=0;i<size;i++)
		{
			for(int j=0;j<size;j++)
			{
				map<<-72+i*0.75<< "\t"<<72-j*0.75<<"\t"<<pudlo[i][j]<<std::endl;
			}
			map<<std::endl;
		}

	map.close();

	plik.close();
}


}

int main()
{
	zad1a();
	zad1();
	zad2();
	
	return 0;
}
