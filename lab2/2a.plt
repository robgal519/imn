reset

# Pliki do zadania 1:
                            # pliki zawierające 2 kolumny:
                            # 1: aktualny numer iteracji it,   2: a(it)
file_1_a_1 = "z1_0.600000.dat"     # dla omega = 0.60
file_1_a_2 = "z1_0.800000.dat"     # dla omega = 0.80
file_1_a_3 = "z1_1.000000.dat"     # dla omega = 1.00
file_1_a_4 = "z1_1.500000.dat"     # dla omega = 1.50
file_1_a_5 = "z1_1.900000.dat"     # dla omega = 1.90
file_1_a_6 = "z1_1.950000.dat"     # dla omega = 1.95
file_1_a_7 = "z1_1.990000.dat"     # dla omega = 1.99

                            # plik zawierający 3 kolumny:
file_1_fi  = "z1_.dat"       # 1: x,   2: y,   3: fi(x,y)


# Pliki do zadania 2:
                            # pliki zawierające 3 kolumny:
                            # 1: x,   2: y,   3: fi(x,y)
file_2_32_fi = "k32.map"    # dla k = 32
file_2_16_fi = "k16.map"    # dla k = 16
file_2_8_fi  =  "k8.map"    # dla k = 8
file_2_4_fi  =  "k4.map"    # dla k = 4
file_2_2_fi  =  "k2.map"    # dla k = 2
file_2_1_fi  =  "k1.map"    # dla k = 1

                            # pliki zawierające 2 kolumny:
                            # 1: aktualny numer iteracji it,   2: a(it)
file_2_32_a  = "k32.dat"    # dla k = 32
file_2_16_a  = "k16.dat"    # dla k = 16
file_2_8_a   =  "k8.dat"    # dla k = 8
file_2_4_a   =  "k4.dat"    # dla k = 4
file_2_2_a   =  "k2.dat"    # dla k = 2
file_2_1_a   =  "k1.dat"    # dla k = 1



### Zadanie 1: a

set term pngcairo size 1000,600 enhanced
set out "1_a.png"

set xlabel "iteracja"
set ylabel "a"

set format y "%.2f"

set logscale x

p file_1_a_1 u 1:2 w l lw 3 t "ω = 0.60", \
  file_1_a_2 u 1:2 w l lw 3 t "ω = 0.80", \
  file_1_a_3 u 1:2 w l lw 3 t "ω = 1.00", \
  file_1_a_4 u 1:2 w l lw 3 t "ω = 1.50", \
  file_1_a_5 u 1:2 w l lw 3 t "ω = 1.90", \
  file_1_a_6 u 1:2 w l lw 3 t "ω = 1.95", \
  file_1_a_7 u 1:2 w l lw 3 t "ω = 1.99"
  
unset logscale x
  
### Zadanie 1: ϕ

set term pngcairo size 800,600 enhanced
set out "1_fi.png"

set xlabel "x"
set ylabel "y"
set cblabel "ϕ"

set xrange [-72:72]
set yrange [-72:72]

set size ratio -1

set format x "%.0f"
set format y "%.0f"
set format cb "%.2f"

p file_1_fi u 1:2:3 w image notitle


### Zadanie 2: ϕ

set term pngcairo size 1200,600 enhanced
set out "2_fi.png"

set multiplot layout 2,3

unset xlabel
unset ylabel
unset cblabel

set xrange [-72:72]
set yrange [-72:72]

set xtics -80,40,80
set ytics -80,40,80

set size ratio -1

set format cb "%.2f"

set title "k = 32"
p file_2_32_fi u 1:2:3 w image notitle

set title "k = 16"
p file_2_16_fi u 1:2:3 w image notitle

set title "k = 8"
p file_2_8_fi u 1:2:3 w image notitle

set title "k = 4"
p file_2_4_fi u 1:2:3 w image notitle

set title "k = 2"
p file_2_2_fi u 1:2:3 w image notitle

set title "k = 1"
p file_2_1_fi u 1:2:3 w image notitle

unset multiplot


### Zadanie 2: a

set term pngcairo size 1000,600 enhanced
set out "2_a.png"

set size noratio

set autoscale xy

set xtics auto
set ytics auto

set format y "%.3f"

set xlabel "iteracja"
set ylabel "a"

p file_2_32_a u 1:2 w l lw 3 t "k = 32", \
  file_2_16_a u 1:2 w l lw 3 t "k = 16", \
  file_2_8_a  u 1:2 w l lw 3 t "k = 8", \
  file_2_4_a  u 1:2 w l lw 3 t "k = 4", \
  file_2_2_a  u 1:2 w l lw 3 t "k = 2", \
  file_2_1_a  u 1:2 w l lw 3 t "k = 1"
