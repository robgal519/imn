#ifndef Lab5_hpp
#define Lab5_hpp

#include <vector>
#include <string>
typedef std::vector<std::vector<double>> Vector2D;
typedef std::vector<std::vector<bool>> Map2D;
int mod(int a, int b);

class Zad1{
  const double Q;
  const double mu;
  const double dx;
  const double dy;
  const int size_x;
  const int size_y;
  const double y_min;
  const double y_max;
  double max;
  double dt;

  Vector2D u_xy,v_xy;
  Map2D map;
  Vector2D rho[3]; // 0 - prev value 1 - current value 2 - value calculated



  void fillSpeed();
  void initializeRho();
  void findMax();
  void leapFrog();
  double integral();
  double pakiet();
  
  void printVector(const Vector2D& vec, std::string name);
  void laxa();

public:
  /**
   * setts all critical constants and prepares arrays
   *
   */
  Zad1(double Q, double mu,
       double dx, double dy,
       int size_x, int size_y );
 void zad2(FILE* file);
  void zad3(FILE* file);
};

#endif
