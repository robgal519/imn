#include "../lib/lab5.hpp"
#include <cmath>
#include <cstdio>
#include <fstream>
int mod(int a, int b){
  if(a<0){
    return b-(-a%b);
  }
  return a%b;
}

void Zad1::printVector(const Vector2D& vec, std::string name){
  std::fstream file(name,std::fstream::out);

  for(int x=0; x<(int)vec.size();x++){
    for(int y=0; y<(int) vec[0].size();y++){
        file <<x*dx<<"\t"<<y*dy<<"\t"<< vec[x][y] << std::endl;
      }
      file<<std::endl;
    }
}

void Zad1::fillSpeed(){
  for(int x=0;x<size_x;x++)
    {
      for(int y=0;y<size_y;y++)
        {
          u_xy[x][y] = Q/(2*mu)*(y*dy-y_min)*(y*dy-y_max);
          v_xy[x][y]=0;
          map[x][y]=1;
        }
    }
}

void Zad1::findMax(){
  max = u_xy[0][0];
  for(int x=0;x<size_x;x++)
    {
      for(int y=0;y<size_y;y++)
        {
          double local_max = sqrt(pow(u_xy[x][y],2)+pow(v_xy[x][y],2));
          if(max < local_max)
            max = local_max;
        }
    }
}
double initRho(double x, double y){
  return exp(-25 * (pow(x-0.4,2)+ pow(y-0.45,2)));
}
void Zad1::initializeRho(){
  for(int x=0;x<size_x;x++)
    {
      for(int y=0;y<size_y;y++)
        {if(!map[x][y])
            {
              rho[0][x][y] = 0;
              rho[1][x][y] = 0;
              rho[2][x][y] = 0;
            }
          rho[0][x][y] = initRho(x*dx,y*dy);
          rho[1][x][y] = initRho(x*dx -u_xy[x][y]*dt,y*dy-v_xy[x][y]*dt);
        }
    }
}
void Zad1::leapFrog(){

  for(int x=0;x<size_x;x++)
    {
      for(int y=1;y<size_y-1;y++)
        {if(!map[x][y])
            continue;
          rho[2][x][y] =
            rho[0][x][y] -
            dt*(u_xy[x][y] *
                (rho[1][mod(x+1,size_x)][y] -
                 rho[1][mod(x-1,size_x)][y])/dx +
                v_xy[x][y]*
                (rho[1][x][y+1]-rho[1][x][y-1])/dy);
        }
    }
}

double Zad1::integral(){
  double result =0;
  for(int x=0;x<size_x;x++)
    {
      for(int y=1;y<size_y-1;y++)
        {if(!map[x][y])
            continue;

         result += rho[1][x][y]*dx*dy;
        }
    }
  return result;
}
double Zad1::pakiet(){
  double result=0;
  for(int x=0; x<size_x; x++)
    {
      for(int y=1;y<size_y-1;y++)
        {
          result += x*dx*rho[1][x][y]*dx*dy;
        }
    }
  return result/integral();
}
Zad1::Zad1(double Q, double mu,
           double dx, double dy,
           int size_x, int size_y):Q(Q),
                                   mu(mu),
                                   dx(dx),
                                   dy(dy),
                                   size_x(size_x),
                                   size_y(size_y),
                                   y_min(0.0),
                                   y_max(0.9){

  u_xy = Vector2D(size_x,std::vector<double>(size_y));
  v_xy = Vector2D(size_x,std::vector<double>(size_y));
  map = Map2D(size_x,std::vector<bool>(size_y));
  
  for(auto &i: rho)
    i = Vector2D(size_x,std::vector<double>(size_y));

  fillSpeed();
  findMax();
  dt = dx/(4*max);
  initializeRho();
  double t=0;
  int i=0;
  double eps = dt/2;
  std::fstream file("1.dat",std::fstream::out);
    while(t<100)
    {
      leapFrog();
      rho[0].swap(rho[1]);
      rho[1].swap(rho[2]);

      if(fabs(t-0) <eps  ||
         fabs(t-5)<eps ||
         fabs(t-10)<eps ||
         fabs(t-15)<eps ||
         fabs(t-20)<eps ||
         fabs(t-99)<eps)
        printVector(rho[1], "1_"+std::to_string(i++)+".map");
      
      file << t << "\t" <<integral() <<"\t"<<pakiet()<<std::endl;
      t+=dt;
     }
}

void Zad1::zad2(FILE * file){
  int x,y,m;
  double u,v;
  while(!feof(file))
    {
      fscanf(file,"%d\t%d\t%lf\t%lf\t%d",&x,&y,&u,&v,&m);
      u_xy[x][y]=u;
      v_xy[x][y]=v;
      map[x][y]=m;
    }

  findMax();
  dt = dx/(4*max);
  initializeRho();
  double t=0;
  int i=0;
  double eps = dt/2;
  std::fstream f("2.dat",std::fstream::out);
  while(t<100)
    {
      leapFrog();
      rho[0].swap(rho[1]);
      rho[1].swap(rho[2]);

      if(fabs(t-0) <eps  ||
         fabs(t-5)<eps ||
         fabs(t-10)<eps ||
         fabs(t-15)<eps ||
         fabs(t-20)<eps ||
         fabs(t-99)<eps)
        printVector(rho[1], "2_"+std::to_string(i++)+".map");
      
      f << t << "\t" <<integral() <<"\t"<<pakiet()<<std::endl;
      t+=dt;
     }
  f.close();
}

void Zad1::laxa(){
  for(int x=0;x<size_x;x++)
    {
      for(int y=1;y<size_y-1;y++)
        {if(!map[x][y])
            continue;
          rho[2][x][y] = (rho[1][mod(x-1,size_x)][y]+rho[1][mod(x+1,size_x)][y]+rho[1][x][y-1]+rho[1][x][y+1])/4 -
            dt*(
                u_xy[x][y]*(rho[1][mod(x+1,size_x)][y] - rho[1][mod(x-1,size_x)][y])/(2*dx)+
                v_xy[x][y]*(rho[1][x][y+1]-rho[1][x][y-1])/(2*dy)
                );
    }
}
}

void Zad1::zad3(FILE* file)
{
  int x,y,m;
  double u,v;
  while(!feof(file))
    {
      fscanf(file,"%d\t%d\t%lf\t%lf\t%d",&x,&y,&u,&v,&m);
      u_xy[x][y]=u;
      v_xy[x][y]=v;
      map[x][y]=m;
    }

  findMax();
  dt = 0.5*dx/(4*max);
  for(int x=0;x<size_x;x++)
    {
      for(int y=0;y<size_y;y++)
        {
          rho[0][x][y]=0;
          rho[1][x][y]=0;
          rho[2][x][y]=0;
        }
    }
  initializeRho();
  double t=0;
  int i=0;
  double eps = dt/2;
  std::fstream f("3.dat",std::fstream::out);
  while(t<100)
    {
      laxa();
      rho[0].swap(rho[1]);
      rho[1].swap(rho[2]);

      if(fabs(t-0) <eps  ||
         fabs(t-5)<eps ||
         fabs(t-10)<eps ||
         fabs(t-15)<eps ||
         fabs(t-20)<eps ||
         fabs(t-99)<eps)
        printVector(rho[1], "3_"+std::to_string(i++)+".map");
      
      f << t << "\t" <<integral() <<"\t"<<pakiet()<<std::endl;
      t+=dt;
    }
  f.close();
}
