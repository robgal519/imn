set term png
set xzeroaxis

set out "zad1a.png"
set y2tic
set ylabel "u(t)"
set y2label "e(t)"
set xlabel "t [0: 4*PI]"
plot  "zad1a.txt" u 1:2 axis x1y1 w l t"Numeric",\
"" u 1:3 axis x1y1 w l t"Analitic",\
"" u 1:4 axis x1y2 w l t"Error" lt rgb "red"

set out "zad1b.png"
set xlabel "t [0: 4*PI]"
set ylabel "e(t)"
plot "zad1b.txt" u 1:3 w l t"Error" lt rgb "red"


set out "zad2a.png"
set xlabel "t [0: 4*PI]"
set ylabel "u(t)"
set y2label "e(t)"
plot "zad2.txt" u 1:2 axis x1y1 w l t"Numeric",\
"" u 1:3 axis x1y1 w l t"Analitic",\
"" u 1:4 axis x1y2 w l t"Error" lt rgb "red"

set out "zad2b.png"
set xlabel "t [0:4*PI]"
set ylabel "u(t)"
set y2label ""
plot "zad1a.txt" u 1:4 w l t"Jawny",\
"zad2.txt" u 1:4 w l t"Niejawny"



set out "zad3_iter.png"
set xlabel "t [0:4*PI]"
set ylabel "iteracje"
set y2label ""
plot "zad3_iter.txt" u 1:2 w lp t"Iteracje wzoru Newtona"

set out "zad3.png"
set xlabel "t [0:4*PI]"
set ylabel "u(t)"
set y2label ""
plot "zad1a.txt" u 1:4 w l t"Jawny",\
"zad2.txt" u 1:4 w l t"Niejawny",\
"zad3.txt" u 1:4 w l t"Newton"


set out "zad4.png"
set xlabel "t [0:4*PI]"
set ylabel "u(t)"
set y2label "e(t)"
plot "zad4.txt" u 1:2 w l t"numeric" ,\
"" u 1:3 w l t"analitic",\
"" u 1:4 w l axis x1y2 t"error" lt rgb "red"

set out "zad4a.png"
set xlabel "t [0:4*PI]"
set ylabel "Euler e(t)"
set y2label "Richardson e(t)"
plot "zad4.txt" u 1:4 w l axis x1y2 t"Richardson",\
"zad1a.txt" u 1:4 w l t"Euler"

set out "zad5.png"
set xlabel "t [0:4*PI]"
set ylabel "u(t)"
set y2label "dT"
plot "zad5.txt" u 1:2 w l t"numeric" ,\
"" u 1:3 w l t"analitic" ,\
"" u 1:5 w l axis x1y2 t"dT(t)" lt rgb "red"

set out "zad5a.png"
set xlabel "t [0:4*PI]"
set ylabel "dT"
set y2label "e(t)"
plot "zad5.txt" u 1:5 w l t"dT(t)",\
"" u 1:4 w l axis x1y2 t"e(t)"

set out "zad5b.png"
set xlabel "t [0:4*PI]"
set ylabel "e(t)"
set y2label ""
plot "zad1a.txt" u 1:4 w l t"Jawny Euler",\
"zad5.txt" u 1:4 w l t"automatyczny dobór dT"


set out "zad6a.png"
set xlabel "t [0:4*PI]"
set ylabel "e(t)"
plot "zad6.txt" u 3:4 w l t"e(t)"

