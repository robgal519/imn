#include <iostream>
#include <cmath>
#include <cstdio>

#define PI 3.14159265358979323846
double t_min = 0;
double t_max = 4*PI;

double foo(double t, double u);
double fooDu(double t);
double result(double t);
double RK4(double un_1, double dT, double tn_1,double foo(double t,double u));

void zad1a();
void zad1b();
void zad2();
void zad3();
void zad4();
void zad5();
void zad6();
int main()
{
  zad1a();
  zad1b();
  zad2();
  zad3();
  zad4();
  zad5();
  zad6();
}

double foo(double t, double u)
{
  return u * cos( t );
}

double fooDu(double t )
{
  return cos( t );
}

double result(double t)
{
  return exp(sin(t));
}

void zad1a()
{
  double u_n , u_n_1, t , dT = PI/400.0 ;
  FILE * file = fopen("zad1a.txt","w");
  t = dT;
  u_n_1 = 1;

  while(t<=t_max)
     {
      u_n = u_n_1 + (dT * foo(t-dT,u_n_1));

     double dok = result(t);
      fprintf(file,"%f\t%f\t%f\t%f\n", t , u_n , dok , dok - u_n );

      t+=dT;
      u_n_1 = u_n;
    }
  fclose(file);
}

void zad1b()
{
  FILE * file = fopen("zad1b.txt","w");

  for(int i=10;i>0;i--)
    {
      double dT = PI/(10*i);
      double u_n, t = dT, u_n_1 = 1;

      double dok;
      while(t<PI+dT-10e-5)
        {
          u_n = u_n_1 + (dT * foo(t-dT,u_n_1));
          dok = result(t);
          t+=dT;
          u_n_1 = u_n;
        }
      fprintf(file , "%f\t%f\t%f\t%f\n", dT , u_n , dok - u_n,t-dT );
    }
  fclose(file);
}

void zad2()
{
  double u_n , u_n_1, t , dT = PI/400.0 ;
  FILE * file = fopen("zad2.txt","w");
  t = dT;
  u_n_1 = 1;

  while(t<=t_max)
    {
      u_n = u_n_1 /(1-dT*cos(t));
      double dok = result(t);
      fprintf(file,"%f\t%f\t%f\t%f\n", t , u_n , dok , dok - u_n );

      t+=dT;
      u_n_1 = u_n;
    }
  fclose(file);
}

void zad3()
{
  double u_n , u_n_1, t , dT = PI/400.0 ;
  FILE * file = fopen("zad3.txt","w");
  FILE * iter = fopen("zad3_iter.txt","w");
  t = dT;
  u_n_1 = 1;

  while(t<=t_max)
    {
      double eps = 10000;
      int it =0;
      double mi, mi_1=u_n_1;
      while(eps > 0.000000000001 && it <50)
        {
          mi = mi_1 - ((mi_1 - u_n_1 - dT*foo(t,mi_1))/(1-dT*fooDu(t)));
          eps = std::fabs(mi - mi_1);
          it ++;
          mi_1 = mi;
        }

      u_n = mi;
      fprintf(iter,"%f\t%d\n",t,it);
      it=0;
      double dok = result(t);
      fprintf(file,"%f\t%f\t%f\t%f\n", t , u_n , dok , dok - u_n );

      t+=dT;
      u_n_1 = u_n;
    }
  fclose(file);
  fclose(iter);
}

void zad4()
{
  double u_n , u_n_1, t , dT = PI/400.0 ;
  FILE * file = fopen("zad4.txt","w");

  t = 2*dT;
  u_n_1 = 1;
  double u_1, u_12, u_2;

  while(t<=t_max)
    {
      u_1 = u_n_1 + (2*dT * foo(t-2*dT,u_n_1));
      u_12 = u_n_1 + (dT * foo(t-2*dT,u_n_1));

      u_2 =u_12 + (dT * foo(t-dT,u_12));
      u_n = u_2*2 - u_1;

      double dok = result(t);
      fprintf(file,"%f\t%f\t%f\t%f\n", t , u_n , dok , dok - u_n );

      t+=2*dT;
      u_n_1 = u_n;
    }
  fclose(file);
}

void zad5()
{
  double S = 0.75;
  double tol = 1e-5;
  double E;
  double u_n , u_n_1, t , dT = PI*5.00 ;
  FILE * file = fopen("zad5.txt","w");
  t = 0;
  u_n_1 = 1;
  double u_1, u_12, u_2;
  while(t<=t_max)
    {
      u_1 = u_n_1 + (2*dT * foo(t,u_n_1));

      u_12 = u_n_1 + (dT * foo(t,u_n_1));
      u_2 =u_12 + (dT * foo(t+dT,u_12));

      E=u_2-u_1;

      if(std::fabs(E) < tol)
        {
          t+=2*dT;
          double dok = result(t);
          u_n = u_2;
          fprintf(file,"%f\t%f\t%f\t%f\t%f\n", t , u_n , dok , dok - u_n, dT );
          dT *= sqrt((S*tol)/std::fabs(E));
          u_n_1 = u_n;
        }
      else
        dT *= sqrt((S*tol)/std::fabs(E));
    }
  fclose(file);
}

double f1(double t, double x, double y)
{
  return y;
}
double f2(double t, double x, double y)
{
  return -x;
}


void zad6()
{
  FILE * plik = fopen("zad6.txt","w");
  double kx[4];
  double ky[4];

  double t_p, t, x_p, y_p, dT, x, y;

  t_p = t_min;
  dT = 0.1;
  x_p = 0;
  y_p = 1; 
  for(t=t_min+dT; t<t_max ; t+=dT)
    {
      kx[0]= f1(t_p, x_p, y_p);
      ky[0]= f2(t_p, x_p, y_p);

      kx[1]= f1(t_p + dT/2.0, x_p + dT*kx[0]/2.0, y_p + dT*ky[0]/2.0);
      ky[1]= f2(t_p + dT/2.0, x_p + dT*kx[0]/2.0, y_p + dT*ky[0]/2.0);

      kx[2]= f1(t_p + dT/2.0, x_p + dT*kx[1]/2.0, y_p + dT*ky[1]/2.0);
      ky[2]= f2(t_p + dT/2.0, x_p + dT*kx[1]/2.0, y_p + dT*ky[1]/2.0);

      kx[3]= f1(t_p + dT, x_p + dT*kx[2], y_p + dT*ky[2]);
      ky[3]= f2(t_p + dT, x_p + dT*kx[2], y_p + dT*ky[2]);

      x = x_p + (dT * (kx[0]+ 2*kx[1] + 2*kx[2] + kx[3]))/6.0;
      y = y_p + (dT * (ky[0] + 2*ky[1] + 2*ky[2] + ky[3]))/6.0;

      fprintf(plik,"%f\t%f\t%f\t%f\n",x,y,t,sin(t)-x);

      x_p = x;
      y_p = y;
      t_p = t;

    }
  fclose(plik);
}
