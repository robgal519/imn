(TeX-add-style-hook
 "raport"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("geometry" "margin=2cm")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "polski"
    "inputenc"
    "geometry"
    "graphicx"
    "amsmath"
    "float"))
 :latex)

