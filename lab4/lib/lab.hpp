#ifndef lab04_h
#define lab04_h
#include <vector>
#include <fstream>

template <typename T>
using Vector2D = std::vector<std::vector<T>>;

struct Settings{
  double y_min,y_max,dx, dy, Q, mu,rho;
};

struct Point{
  Point(int x, int y):X(x),Y(y){};
  int X;
  int Y;
};

template <typename t>
void initZeroVector2D(Vector2D<t>& vector, int x, int y);

double startPsi(int y,Settings set);
double startZeta(int y, Settings set);
void prepareZad1(Vector2D<double>& psi, Settings set, double (*foo)(int y, Settings set));
void relaksacja(Vector2D<double>& Psi, Vector2D<double>& Zeta, Vector2D<bool> map);


double calculatePsi(int x, int y, const Vector2D<double>& Psi, const Vector2D<double>& Zeta, Settings set);
double calculateZeta(int x, int y, const Vector2D<double>& Psi, const Vector2D<double>& Zeta);
void zad1(Settings set);
void zad2(const Settings& _set, std::string name);

#endif
