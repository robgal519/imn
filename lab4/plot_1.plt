reset
                                 # plik zawierające 4 kolumny:
file_1_dzeta = "1_dzeta.out"     # 1: y    2: dzeta0(y)   3: dzeta(50*dx, y)   4: dzeta(250*dx, y)

                                 # plik zawierające 4 kolumny:
file_1_psi   = "1_psi.out"       # 1: y    2: psi0(y)     3: psi(50*dx, y)     4: psi(250*dx, y)

                                 # plik zawierające 4 kolumny:
file_1_u     = "1_u.out"         # 1: y    2: u0(y)       3: u(50*dx, y)


################################

set term pngcairo size 1000,600 enhanced

set xlabel "y"

################################

set out "fig_1_dzeta_50.png"
set ylabel "dzeta"
set title "dzeta dla i = 50"
p file_1_dzeta u 1:2 w l lw 3 t "analityczne", "" u 1:3 w p lw 3 t "numeryczne"

set out "fig_1_dzeta_250.png"
set ylabel "dzeta"
set title "dzeta dla i = 250"
p file_1_dzeta u 1:2 w l lw 3 t "analityczne", "" u 1:4 w p lw 3 t "numeryczne"

set out "fig_1_psi_50.png"
set ylabel "psi"
set title "psi dla i = 50"
p file_1_psi u 1:2 w l lw 3 t "analityczne", "" u 1:3 w p lw 3 t "numeryczne"

set out "fig_1_psi_250.png"
set ylabel "psi"
set title "psi dla i = 250"
p file_1_psi u 1:2 w l lw 3 t "analityczne", "" u 1:4 w p lw 3 t "numeryczne"

set out "fig_1_u_50.png"
set ylabel "u"
set title "u dla i = 50"
p file_1_u u 1:2 w l lw 3 t "analityczne", "" u 1:3 w p lw 3 t "numeryczne"