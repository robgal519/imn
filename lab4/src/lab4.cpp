#include "../lib/lab.hpp"
#include <cmath>
#include <iostream>

template <typename t>
void initZeroVector2D(Vector2D<t>& vector,int x, int y){

  vector.resize(x);
  for(auto& i:vector ){
    i.resize(y);
    for(int a=0;a<y;a++)
      i[a]=0;
    }

}

double startZeta(int y, Settings set)
{
  return set.Q / (2.0 * set.mu) * (2.0 * y * set.dy - set.y_min - set.y_max);
}

double startPsi(int y, Settings set)
{
  return set.Q/(2.0 * set.mu) * (pow(y*set.dy,3)/3 - pow(y*set.dy,2)/2 * (set.y_min+ set.y_max) + set.y_max*set.y_min * y * set.dy );
}

void prepareZad1(Vector2D<double>& vec,Settings set, double (*foo)(int y, Settings set))
{
  for(int x=0;x<(int)vec.size();x++)
    {
      vec[x][0] = foo(0,set);
      vec[x][vec[0].size()-1] = foo(vec[0].size()-1, set);
    }

  for(int y=0;y<(int)vec[0].size();y++)
    {
      vec[0][y] = vec[vec.size()-1][y] = foo(y, set);
    }
}

double calculatePsi(int x, int y, const Vector2D<double>& Psi, const Vector2D<double>& Zeta, Settings set){
  return (Psi[x+1][y]+
          Psi[x-1][y]+
          Psi[x][y-1]+
          Psi[x][y+1] -
          Zeta[x][y]*pow(set.dx,2))/4.0;
}
double calculateZeta(int x, int y, const Vector2D<double>& Psi, const Vector2D<double>& Zeta){
  return (Zeta[x+1][y]+
          Zeta[x-1][y]+
          Zeta[x][y-1]+
          Zeta[x][y+1])/4.0 - (
                               (Psi[x][y+1]-Psi[x][y-1])*
                               (Zeta[x+1][y]-Zeta[x-1][y])
                               -
                               (Psi[x+1][y]- Psi[x-1][y])* 
                               (Zeta[x][y+1]-Zeta[x][y-1])
                               )/16.0;
}

void relaksacja(Vector2D<double>& Psi, Vector2D<double>& Zeta, Vector2D<bool> map,Settings set)
{
  int size_x = Psi.size();
  int size_y = Psi[1].size();
  for(int x=0;x<size_x;x++){
    for(int y=0;y<size_y;y++){
      if(!map[x][y]){
       Zeta[x][y] = calculateZeta( x, y,Psi,Zeta);
       Psi[x][y] = calculatePsi( x, y, Psi,Zeta,set);
      }

    }
 }
}
template <typename t>
void printArray(Vector2D<t> array)
{
  for(int y=90;y>=0;y--)
    {
      for(int x=0;x<301;x++)
        std::cout << array[x][y]<<" ";
      std::cout<<std::endl;
    }
}

void zad1(Settings set)
{
  Vector2D<double> zeta,psi;
  initZeroVector2D(zeta,301, 91);
  initZeroVector2D(psi, 301, 91);

  prepareZad1(zeta,set, startZeta);
  prepareZad1(psi, set, startPsi);

  Vector2D<bool> map;
  initZeroVector2D(map, 301, 91);
  for(int x=0;x<301;x++)
    {
      map[x][0] = map [x][90] =1 ;
    }


  for(int y=0; y<91;y++)
    map[0][y] = map[300][y] = 1;

 

  double tol = 1e-7;
  int it=0;
  double prev=0, cur=1;
  double prev1=0,cur1=1;
   
  while(it<100|| (std::fabs(prev-cur)>tol || std::fabs(prev1-cur1)>tol))
    {
      prev = cur;
      prev1 = cur1;
      relaksacja(psi, zeta, map, set);
      cur = psi[145][45];
        cur1 = zeta[145][45];
      it ++;
      // std::cout << cur-prev<<"\n";
    }
  std::fstream file,file2,file3;
  file.open("1_dzeta.out",std::fstream::out);
  file2.open("1_psi.out", std::fstream::out);
  file3.open("1_u.out", std::fstream::out);

  for(int j = 0; j< 91; j++)
    {
      file<<j<<"\t"<<startZeta(j,set)<<"\t"<<zeta[50][j]<<"\t"<<zeta[250][j]<<std::endl;
      file2<<j<<"\t"<<startPsi(j,set)<<"\t"<<psi[50][j]<<"\t"<<psi[250][j]<<std::endl;

    }
  for(int j=0;j<90;j++)
    file3<<j<<"\t"<<(psi[0][j+1]-psi[0][j])/0.01<<"\t"<< (psi[50][j+1]-psi[50][j])/0.01<<std::endl;
  file.close();
  file2.close();
  file3.close();
}

void preparePsiZad2(Vector2D<double>& Psi, Vector2D<double>& Zeta, Vector2D<bool>map, Settings set)
{
  for(int x=0;x<301;x++)
    {
      for(int y=0;y<91;y++)
        {
          Psi[x][y] = startPsi(y,set);
          Zeta[x][y] = startZeta(y,set);
        }
    }

  for(int x=1;x<300;x++)
    {
      for(int y=1;y<90;y++)
        {
          if(map[x][y])
            { Psi[x][y]= Psi[x][90];
          Zeta[x][y] = Zeta[x][90];

            }
        }
    }
}


void preRelaxZeta(Vector2D<double>& Zeta,Vector2D<double> Psi, Vector2D<bool> map, Settings& set)
{
  for(int x=0;x<85;x++)
    {
      Zeta[x][90] = 2.0*(Psi[x][90-1]-Psi[x][90])/pow(set.dx,2);
    }
  for(int x=116;x<301;x++)
    {
      Zeta[x][90] = 2.0*(Psi[x][90-1]-Psi[x][90])/pow(set.dx,2);
    }
  for(int x=85;x<101;x++)
    {
      Zeta[x][70] = 2.0*(Psi[x][70-1]-Psi[x][70])/pow(set.dx,2);
    }
  for(int x=101;x<116;x++)
    {
      Zeta[x][50] = 2.0*(Psi[x][50-1]-Psi[x][50])/pow(set.dx,2);
    }
  for(int x=0;x<301;x++)
    {
      Zeta[x][0] = 2.0*(Psi[x][0+1]-Psi[x][0])/pow(set.dx,2);
    }
  for(int y=70;y<91;y++)
    {
      Zeta[85][y] = 2.0*(Psi[85-1][y]-Psi[85][y])/pow(set.dx,2);
    }
  for(int y = 50;y<70;y++)
    {
      Zeta[101][y] = 2.0*(Psi[101-1][y]-Psi[101][y])/pow(set.dx,2);
    }
  for(int y=50;y<91;y++)
    {
      Zeta[116][y] = 2.0*(Psi[116+1][y]-Psi[116][y])/pow(set.dx,2);
    }
  Zeta[70][85] =  0.5* ( 2.0*(Psi[70][85-1]-Psi[70][85])/pow(set.dx,2)+2.0*(Psi[70-1][85]-Psi[70][85])/pow(set.dx,2));
  Zeta[101][50] = 0.5 * ( 2.0*(Psi[101-1][50]-Psi[101][50])/pow(set.dx,2)+ 2.0*(Psi[101][50-1]-Psi[101][50])/pow(set.dx,2));
  Zeta[116][50] = 0.5 *(2.0*(Psi[116][50-1]-Psi[116][50])/pow(set.dx,2) + 2.0*(Psi[116+1][50]-Psi[116][50])/pow(set.dx,2));
}
void zad2(const Settings& _set, std::string name)
{
  Settings set(_set);

  Vector2D<double>psi,zeta;
  Vector2D<bool> map;

  initZeroVector2D(zeta,301, 91);
  initZeroVector2D(psi, 301, 91);


  initZeroVector2D(map, 301, 91);
  for(int x=0;x<301;x++)
    {
      map[x][0] = map [x][90] =1 ;
    }


  for(int y=0; y<91;y++)
    map[0][y] = map[300][y] = 1;

  for(int x = 85;x<117;x++)
    {
      for(int y=70;y<91;y++)
        map[x][y]=1;
    }
  for(int x=101;x<117;x++)
    {
      for(int y=50;y<70;y++)
        map[x][y]=1;
    }


  
  preparePsiZad2(psi,zeta,map,set);


  double tol = 1e-7;
  int it=0;
  double prev=0, cur=1;
  double prev1=0,cur1=1;
   
  while(it<100|| (std::fabs(prev-cur)>tol || std::fabs(prev1-cur1)>tol))
    {
      prev = cur;
      prev1 = cur1;
      preRelaxZeta(zeta,psi,map,set);
      relaksacja(psi, zeta, map, set);
      cur = psi[145][45];
      cur1 = zeta[145][45];
      it ++;
      // std::cout << cur-prev<<"\n";
    }

  std::fstream file, file2;
  file.open(name+"_psi.out",std::fstream::out);
  for(int x=0;x<301;x++)
    {
      for(int y=0;y<91;y++)
        {
          file<<x*set.dx<<"\t"<<y*set.dy<<"\t"<< psi[x][y]<<std::endl;
        }
      file<<std::endl;
    }
  file2.open(name+"_uv.out",std::fstream::out);
  for(int x=0;x<300;x++)
    {
      for(int y=0;y<90;y++)
        {
          file2<<x*set.dx<<"\t"<<y*set.dx<<"\t"<<(psi[x][y+1]-psi[x][y])/set.dy<< "\t" <<-1.0 * (psi[x+1][y]-psi[x][y])/set.dx<<std::endl; 
        }
      file2<<std::endl;
    }
  file.close();
  file2.close();
  
}

