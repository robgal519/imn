#include "../lib/lab.hpp"
#include <iostream>
int main(int argc, char **argv)
{

  Settings set;
  set.Q=-1;
  set.dx = set.dy = 0.01;
  set.mu = set.rho =1;
  set.y_min = 0;
  set.y_max = 0.9;

  zad1(set);
  std::cout<< " 1 done \n";
  zad2(set,"2_Q1");
  std::cout<< " 2 done \n";

  set.Q =-150;
  zad2(set,"2_Q2");

  std::cout<< " 3 done \n";

  set.Q = -400;
  zad2(set,"2_Q3");


  return 0;
}

