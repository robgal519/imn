reset
                                 # plik zawierające 3 kolumny:
                                 # 1: x   2: y   3: psi(x,y)
file_2_Q1_psi = "2_Q1_psi.out"   # Q = -1
file_2_Q2_psi = "2_Q2_psi.out"   # Q = -150
file_2_Q3_psi = "2_Q3_psi.out"   # Q = -400

                                 # pliki zawierające 4 kolumny:
                                 # 1: x   2: y   3: u(x,y)   4: v(x,y)
file_2_Q1_uv = "2_Q1_uv.out"     # Q = -1
file_2_Q2_uv = "2_Q2_uv.out"     # Q = -150
file_2_Q3_uv = "2_Q3_uv.out"     # Q = -400

################################

# Granice
Q1_u_max = 0.18
Q1_v_max = 0.08
Q2_u_max = 30
Q2_v_max = 12
Q3_u_max = 80
Q3_v_max = 35

################################

lines = 50

set term pngcairo size 1200,500 enhanced

set xlabel "x"
set ylabel "y"

set xtics out nomirror
set ytics out nomirror

set xrange [0:3.0]
set yrange [0:0.9]

set size ratio -1

set object polygon from 0.85,0.90 to 0.85,0.70 to 1.01,0.70 to 1.01,0.50 to 1.16,0.50 to 1.16,0.90 to 0.85,0.90 fs solid border lc rgb "black" fc rgb "gray" front

################################

# Konturowe + kolor: psi

set contour
set cntrparam levels lines
set view map
unset key

do for [i=1:100] {
  set linetype i lc rgb "black"
}

## Q = -1
set out "fig_2_psi_Q1.png"
set title "Q = -1"
sp file_2_Q1_psi u 1:2:3 w pm3d notitle

## Q = -150
set out "fig_2_psi_Q2.png"
set title "Q = -150"
sp file_2_Q2_psi u 1:2:3 w pm3d notitle

## Q = -400
set out "fig_2_psi_Q3.png"
set title "Q = -400"
sp file_2_Q3_psi u 1:2:3 w pm3d notitle

################################

# Kolor: u, v

unset contour
set palette model RGB functions (2*gray-1),0,(1-2*gray)

## Q = -1
set out "fig_2_u_Q1.png"
set title "Q = -1"
set cbrange [-Q1_u_max:Q1_u_max]
p file_2_Q1_uv u 1:2:3 w image notitle

set out "fig_2_v_Q1.png"
set title "Q = -1"
set cbrange [-Q1_v_max:Q1_v_max]
p file_2_Q1_uv u 1:2:4 w image notitle

## Q = -150
set out "fig_2_u_Q2.png"
set title "Q = -150"
set cbrange [-Q2_u_max:Q2_u_max]
p file_2_Q2_uv u 1:2:3 w image notitle

set out "fig_2_v_Q2.png"
set title "Q = -150"
set cbrange [-Q2_v_max:Q2_v_max]
p file_2_Q2_uv u 1:2:4 w image notitle

## Q = -400
set out "fig_2_u_Q3.png"
set title "Q = -400"
set cbrange [-Q3_u_max:Q3_u_max]
p file_2_Q3_uv u 1:2:3 w image notitle

set out "fig_2_v_Q3.png"
set title "Q = -400"
set cbrange [-Q3_v_max:Q3_v_max]
p file_2_Q3_uv u 1:2:4 w image notitle