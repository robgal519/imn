reset

file_1  =  "1.dat"   # plik zawierający 3 kolumny:   1: x   2: u(x, t_i)   3: u_analityczne(x, t_i)
                     # dla 9 chwil i (wymagane dwie nowe linie pomiędzy danymi dla kolejnych chwil)
                   
file_2a = "2a.dat"   # pliki zawierający 3 kolumny:  1: t   2: x   3: u(x, t)
file_2b = "2b.dat"
file_2c = "2c.dat"
file_3a = "3a.dat"
file_3b = "3b.dat"
file_3c = "3c.dat"
file_4  =  "4.dat"

file_5  =  "5.dat"   # plik zawierający 2 kolumny:   1: omega   2: <E>

################################

set term pngcairo size 1000,600 enhanced

if (GPVAL_VERSION >= 5.0) set for [i=1:9] linetype i dashtype i
if (GPVAL_VERSION < 5.0)  set termoption dashed

set palette model RGB functions (2*gray-1),0,(1-2*gray)

################################

# Zadanie 1

set xlabel "x"
set ylabel "u"

set xrange [0:1]
set yrange [-1.5:1.5]

set format x "%.1f"
set format y "%.1f"

set key bottom right

set out "ex_1.png"
p file_1 u 1:2 w l lt 1 lw 3 lc rgb "gray" t "numeryczne", \
      "" u 1:3 w l lt 2 lw 3 lc rgb "blue" t "analityczne"

################################

# Zadania 2-4

set xlabel "t"
set ylabel "x"
set cblabel "u"

set xrange [0:4]
set yrange [0:1]
set cbrange [-1:1]

set format cb "%.1f"

set out "ex_2a.png"
p file_2a u 1:2:3 w image notitle

set out "ex_2b.png"
p file_2b u 1:2:3 w image notitle

set out "ex_2c.png"
p file_2c u 1:2:3 w image notitle

set out "ex_3a.png"
p file_3a u 1:2:3 w image notitle

set out "ex_3b.png"
p file_3b u 1:2:3 w image notitle

set out "ex_3c.png"
p file_3c u 1:2:3 w image notitle


set xrange [0:10]
set cbrange [-0.004:0.004]

set format cb "%.3f"

set out "ex_4.png"
p file_4 u 1:2:3 w image notitle

################################

# Zadanie 5

set xlabel "ω"
set ylabel "<E>"

set xrange [0:10]
set yrange [0:*]

set format x "%.0fπ"
set format y "%.1t·10^{%T}"

set out "ex_5.png"
p file_5 u ($1/pi):2 w l lw 3 lc rgb "blue" notitle

################################

set out