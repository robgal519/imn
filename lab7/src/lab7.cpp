#include "../lib/lab7.hpp"
#include <cmath>
#include <fstream>

lab7::lab7(config cfg):cfg(cfg){
  v = std::vector<double>(cfg.netDencity,0);
  u = v;
  a = u;
}

void lab7::setStartValue(){
  for(auto &i: v){
    i=0;
  }
  int i=0;
  for(double x = cfg.XminVal ; x<cfg.XmaxVal; x+=cfg.dX, i++){
    u[i]=sin(M_PI*x) -
      0.5*sin(2*M_PI*x);
    a[i]=-pow(M_PI,2)*cos(M_PI*0)*sin(M_PI*x)+
      2*pow(M_PI,2)*cos(2*M_PI*0)*sin(2*M_PI*x);
  }
}

void lab7::zad1(){
  std::fstream plik("1.dat",std::fstream::out);
  setStartValue();
  std::vector<double> v_pos(cfg.netDencity,0);
 
  for(double t = 0.0;t<=2;t+=cfg.dT)
    {
      int i=0;
      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+=cfg.dX, i++)
        {
         
          v_pos[i]=v[i]+cfg.dT/2.0 * a[i];
          u[i] = u[i]+cfg.dT*v_pos[i];
        }
      i=0;
      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+= cfg.dX, i++){
        if(i!=0 || i!= cfg.netDencity-1)
          a[i] = (u[i-1]+u[i+1] -2*u[i])/pow(cfg.dX,2);
        v[i] = v_pos[i]+cfg.dT/2.0 * a[i];
      }

      if(fmod(t,0.25)<1e-15)
        {for(int i=0;i<cfg.netDencity;i++)
          plik<<i*cfg.dX
              <<"\t"<< u[i]
              <<"\t"<<
            cos(M_PI*t)*sin(M_PI*i*cfg.dX) -
            0.5*cos(2*M_PI*t)*sin(2*M_PI*i*cfg.dX)
              <<std::endl;

      plik<<std::endl<<std::endl;
    }
    }
}
void lab7::zad2(){

  std::fstream file("2a.dat", std::fstream::out);
  for(int i=0; i<cfg.netDencity;i++)
    {
      u[i] = exp(-100*pow(i*cfg.dX-0.5,2));
      v[i] = 0;
      a[i] = 0;
    }
  u[0]=u[cfg.XmaxVal-1]=0;


  std::vector<double> v_pos(cfg.netDencity,0);
  for(double t = 0.0;t<=4;t+=cfg.dT)
    {
      int i=0;
      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+=cfg.dX, i++)
        {
         
          v_pos[i]=v[i]+cfg.dT/2.0 * a[i];
          u[i] = u[i]+cfg.dT*v_pos[i];
        }
      i=0;
      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+= cfg.dX, i++){
        if(i!=0 || i!= cfg.netDencity-1)
          a[i] = (u[i-1]+u[i+1] -2*u[i])/pow(cfg.dX,2);
        v[i] = v_pos[i]+cfg.dT/2.0 * a[i];
      }

      for(int i=0;i<cfg.netDencity;i++)
        file <<t
             <<"\t"
             << i*cfg.dX
             <<"\t"
             <<u[i]
             <<"\n";
      file<<std::endl;


    }
  file.close();


  ////b
  for(int i=0; i<cfg.netDencity;i++)
    {
      u[i] = exp(-100*pow(i*cfg.dX-0.5,2));
      v[i] = 0;
      a[i] = 0;
    }

  std::fstream file2("2b.dat", std::fstream::out);
  for(double t = 0.0;t<=4;t+=cfg.dT)
    {
    
      int i=0;
      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+=cfg.dX, i++)
        {
         
          v_pos[i]=v[i]+cfg.dT/2.0 * a[i];
          u[i] = u[i]+cfg.dT*v_pos[i];
        }
      i=0;
      u[0]=u[1];
      u[cfg.netDencity-1]=u[cfg.netDencity-2];

      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+= cfg.dX, i++){
        if(i!=0 || i!= cfg.netDencity-1)
          a[i] = (u[i-1]+u[i+1] -2*u[i])/pow(cfg.dX,2);
        v[i] = v_pos[i]+cfg.dT/2.0 * a[i];
      }

      for(int i=0;i<cfg.netDencity;i++)
        file2 <<t
             <<"\t"
             << i*cfg.dX
             <<"\t"
             <<u[i]
             <<"\n";
      file2<<std::endl;


    }
  file2.close();


  //// c

  std::fstream file3("2c.dat", std::fstream::out);
  for(int i=0; i<cfg.netDencity;i++)
    {
      u[i] = exp(-100*pow(i*cfg.dX-0.5,2));
      v[i] = 0;
      a[i] = 0;
    }
  u[0]=u[cfg.XmaxVal-1]=0;


  for(double t = 0.0;t<=4;t+=cfg.dT)
    {
      int i=0;
      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+=cfg.dX, i++)
        {
         
          v_pos[i]=v[i]+cfg.dT/2.0 * a[i];
          u[i] = u[i]+cfg.dT*v_pos[i];
        }
      i=0;
      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+= cfg.dX, i++){
        if(i!=0 || i!= cfg.netDencity-1)
          a[i] = (u[i-1]+u[i+1] -2*u[i])/pow(cfg.dX,2) * 1/rho(x);
        v[i] = v_pos[i]+cfg.dT/2.0 * a[i];
      }

      for(int i=0;i<cfg.netDencity;i++)
        file3 <<t
             <<"\t"
             << i*cfg.dX
             <<"\t"
             <<u[i]
             <<"\n";
      file3<<std::endl;


    }
  file3.close();




}

double lab7::rho(double x){
  if(x<cfg.x_0)
    return 1;
  return cfg.rho;
}

void lab7:: z3abc(std::string name, double beta){
  std::fstream file(name, std::fstream::out);
  for(int i=0; i<cfg.netDencity;i++)
    {
      u[i] = exp(-100*pow(i*cfg.dX-0.5,2));
      v[i] = 0;
      a[i] = 0;
    }
  u[0]=u[cfg.XmaxVal-1]=0;

  std::vector<double> u_prev = u;
  std::vector<double> v_pos(cfg.netDencity,0);
  for(double t = 0.0;t<=4;t+=cfg.dT)
    {u_prev = u;
      int i=0;
      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+=cfg.dX, i++)
        {
         
          v_pos[i]=v[i]+cfg.dT/2.0 * a[i];
          u[i] = u[i]+cfg.dT*v_pos[i];
        }
      i=0;
      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+= cfg.dX, i++){
        if(i!=0 || i!= cfg.netDencity-1)
          a[i] = (u[i-1]+u[i+1] -2*u[i])/pow(cfg.dX,2) - 2*beta * (u[i]-u_prev[i])/cfg.dT;
        v[i] = v_pos[i]+cfg.dT/2.0 * a[i];
      }

      for(int i=0;i<cfg.netDencity;i++)
        file <<t
             <<"\t"
             << i*cfg.dX
             <<"\t"
             <<u[i]
             <<"\n";
      file<<std::endl;


    }
  file.close();


}
void lab7::zad3(){
  z3abc("3a.dat", 0.2);
  z3abc("3b.dat", 1);
  z3abc("3c.dat",3);
}
void lab7::zad4(){
  cfg.x_0 = 0.5;
  std::fstream file("4.dat", std::fstream::out);
  for(int i=0; i<cfg.netDencity;i++)
    {
      u[i] = 0;
      v[i] = 0;
      a[i] = 0;
    }
  u[0]=u[cfg.XmaxVal-1]=0;

  std::vector<double> u_prev = u;
  std::vector<double> v_pos(cfg.netDencity,0);
  for(double t = 0.0;t<=10;t+=cfg.dT)
    {u_prev = u;
      int i=0;
      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+=cfg.dX, i++)
        {
                   v_pos[i]=v[i]+cfg.dT/2.0 * a[i];
          u[i] = u[i]+cfg.dT*v_pos[i];
        }
      i=0;
      for(double x = cfg.XminVal;x<cfg.XmaxVal;x+= cfg.dX, i++){
        if(i!=0 || i!= cfg.netDencity-1)
          a[i] = (u[i-1]+u[i+1] -2*u[i])/pow(cfg.dX,2) - 2*1 * (u[i]-u_prev[i])/cfg.dT + cos(M_PI/2 * t)*exp(-1e5*pow(x-cfg.x_0,2));
        v[i] = v_pos[i]+cfg.dT/2.0 * a[i];
      }

      for(int i=0;i<cfg.netDencity;i++)
        file <<t
             <<"\t"
             << i*cfg.dX
             <<"\t"
             <<u[i]
             <<"\n";
      file<<std::endl;


    }
  file.close();



}
void lab7::zad5(){

  cfg.x_0 = 0.5;
  std::fstream file("5.dat", std::fstream::out);
  double E = 0;

  std::vector<double> u_prev = u;
  std::vector<double> v_pos(cfg.netDencity,0);
  for(double omega = 0; omega <10*M_PI; omega += 0.1)
    {
    for(int i=0; i<cfg.netDencity;i++)
      {
        u[i] = 0;
        v[i] = 0;
        a[i] = 0;
      }
    u[0]=u[cfg.XmaxVal-1]=0;

    for(double t = 0.0;t<20;t+=cfg.dT){
      u_prev = u;
        int i=0;
        for(double x = cfg.XminVal;x<cfg.XmaxVal;x+=cfg.dX, i++)
          {
            v_pos[i]=v[i]+cfg.dT/2.0 * a[i];
            u[i] = u[i]+cfg.dT*v_pos[i];
          }
        i=0;
        for(double x = cfg.XminVal;x<cfg.XmaxVal;x+= cfg.dX, i++){
          if(i!=0 || i!= cfg.netDencity-1)
            a[i] = (u[i-1]+u[i+1] -2*u[i])/pow(cfg.dX,2) - 2*1 * (u[i]-u_prev[i])/cfg.dT + cos(omega * t)*exp(-1e5*pow(x-cfg.x_0,2));
          v[i] = v_pos[i]+cfg.dT/2.0 * a[i];
        }




        if(t>16)
          {
            double int1=0, int2 =0;
            for(int p = 1; p<cfg.netDencity-1;p++)
              {
                int1 += v[p]*v[p]*cfg.dX;
                int2 += pow( (u[p+1]-u[p-1])/(2.0*cfg.dX) , 2 ) * cfg.dX;
              }
            E += 0.5*int1+0.5*int2;


          }
      }

    file << omega<<"\t"
         <<E/(20-16)*cfg.dT<<"\n";
    E=0;



  }
  file.close();





}
