#pragma once
#include <vector>
#include <string>
struct config{
  int netDencity = 101;
  double XminVal = 0.0;
  double XmaxVal = 1.0;

  double dX = (XmaxVal - XminVal )/(netDencity*1.0 - 1);
  double dT = 1.0/200;
  double x_0 = 0.75;
  double rho = 3;

};

class lab7{
  config cfg;
  /**
   * u - wychylenie struny
   *
   * v - prędkość struny w danym punkcie
   *
   * a - przyspieszenie
   */
  std::vector<double> u,v,a; 
  void setStartValue();
  double vP(double x, double t);
  double rho(double x);

  void z3abc(std::string name, double beta);
public:
  lab7(config cfg);

  void zad1();
  void zad2();
  void zad3();
  void zad4();
  void zad5();
};
