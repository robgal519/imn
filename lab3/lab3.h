#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <fstream>


typedef std::vector<std::vector<double> > array2D;


void zad1();
void zad2();

/**
 *@param i x value of array
 *@param j y value of array
 *@param array array on wich calculation executes
 *
 *this function apply Phi function on array according to equation (4) from lab3 script (excercise 1)
 */
void varPhi(int i, int j, array2D& array );

/**
 *@param vector input data to calculate avarage value
 */
double avarage(const std::vector<double>& vector); 

/**
 *@param x x coordinates
 *@param y y coordinates
 *@return true if defined point is inside the obsticle 
 */
bool onObstacle(int x, int y);

/**
 *prepares values of the array in the whay that is described in 1 st. excercise
 *
 */
void prepareInitialValue(array2D& array);

/**
 *apply Neuman roule to top edge, and edge of obsticle
 */
void Neuman(array2D& array); 


/**
 *prepares values of array according to description in excercise 2
 *
 */
void prepareInitialValueEx2(array2D& array);

/**
 *@param array will be cleared, and set all values to 0
 *@param x set new x size of array
 *@param y set new y size of array
 *after that you can use array like array[x-1][y-1] ( for dumys like me)
 *
 *array.size() returns x 
 */
void clearAndResizeArray(array2D& array, int x, int y);

double integral(const array2D& array);

void calculatePhi(array2D& array);
