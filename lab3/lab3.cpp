#include "lab3.h"

#include <fstream>

double avarage(const std::vector<double>& vector)
{
  double av = 0;
 
  for(auto& i:vector)
    av += i;
  return av/vector.size();
}


void varPhi(int i, int j, array2D& array)
{
  array[i][j] = avarage({array.at(i-1).at(j),
        array.at(i).at(j-1),
        array.at(i+1).at(j),
        array.at(i).at(j+1)});
}
bool onObstacle(int x,int y)
{
  //separate obsticle into two rectangles
  if(x>85 && x<100)
    {
      if(y>85 && y <100)
        return true;
    }

  if(x>100 &&  x<115)
    {
      if(y>70 && y<100)
        return true;
    }

  return false;
}

void clearAndResizeArray(array2D& array,int x, int y)
{
   array.resize(x);
  for(auto& i:array)
    {
      i.resize(y);
      for(auto& j:i)
        j=0;
    }
}



void prepareInitialValue(array2D& array)
{
  clearAndResizeArray(array,200,100);
  //right and left border
  for(int y=0;y<100;y++)
    {
    array[0][y] = y;
    array[199][y] = y;
    }
  //top and bottom
  for(int x=0;x<200;x++)
    {
      array[x][0]=0;
      if(!onObstacle(x,99))
        array[x][99]=99;
    }
  //obsticle
  for(int y=85;y<100;y++)
    array[85][y] = 99;
  for(int x=84;x<100;x++)
    array[x][85]=99;
  for(int y=70;y<=84;y++)
    array[100][y]=99;
  for(int x=99;x<=115;x++)
    array[x][70]=99;
  for(int y=69;y<100;y++)
    array[115][y]=99;
}

void Neuman(array2D& array)
{
  //top edge
  for(int x=0;x<85;x++)
    {
      array[x][99]= array[x][98];
    }
  for(int x=116;x<200;x++)
    {
      array[x][99]= array[x][98];
    }
  //bottom edge
  for(int x=0;x<array.size();x++)
    {
      array[x][0] = array[x][1];
    }

  // for(int y=0;y<100;y++)
  //   {
  //     array[0][y] = array[1][y];
  //     array[199][y] = array[198][y];
  //   }

  

  //obsticle
  for(int y=85;y<100;y++)
    array[85][y] = array[84][y];
  for(int x=85;x<=100;x++)
    array[x][85]=array[x][84];
  for(int y=70;y<=85;y++)
    array[100][y]=array[99][y];
  for(int x=100;x<=115;x++)
    array[x][70]=array[x][69];
  for(int y=70;y<100;y++)
    array[115][y]=array[116][y];

  array[85][85] = avarage({array[84][85],array[85][84]});
  array[100][70] = avarage({array[99][70],array[100][69]});
  array[115][70] = avarage({array[116][70],array[115][69]});
  array[100][85] = array[99][84];
}



void prepareInitialValueEx2(array2D& array)
{
  clearAndResizeArray(array,200,100);
  for(int x=0;x<200;x++)
    {
      for(int y=0;y<100;y++)
        {
          if(!onObstacle(x,y))
            array[x][y] = x+1;
        }
    }
}


double integral(const array2D& array)
{
  double sum = 0;

  for(int x=1;x<array.size()-1;x++)
    {
      for(int y=1;y<array[x].size()-1;y++)
        {
          if(onObstacle(x+1,y) or onObstacle(x-1,y) or onObstacle(x,y+1))
            continue;
          sum += pow(array[x+1][y]-array[x-1][y],2) +
            pow(array[x][y-1]-array[x][y-1],2);
        }
    }
  return sum/8.0;
}

void calculatePhi(array2D& array)
{
  for(int x=1;x<200-1;x++)
    {
      for(int y=1;y<100-1;y++)
        {
          if(onObstacle(x+1,y) or onObstacle(x-1,y) or onObstacle(x,y+1))
            continue;
          varPhi(x,y,array);
        }
    }
}

void zad1(){
  array2D array;
  
  std::fstream plik, plik2;
  plik2.open("iteracje",std::fstream::out);
  prepareInitialValue(array);

  double prev_a, a;
  int it =0;

  double tol = 1e-6;
  calculatePhi(array);
  prev_a = integral(array);

  calculatePhi(array);
  a=integral(array);
  it =1;

  plik2 << it << "\t" << a<<std::endl;
  while(std::fabs(prev_a-a) > tol &&  it < 1e5)
    {
      prev_a = a;
      it ++;
      calculatePhi(array);
      a=integral(array);
      plik2 << it << "\t" << a<<std::endl;
    }


  plik.open("zad1",std::fstream::out);
  for(int x=0;x<array.size();x++)
    {
      for(int y=0;y<array[0].size();y++)
        plik << x
             <<"\t"
             <<y
             <<"\t"
             <<array[x][y]
             <<"\n";
      plik<<std::endl;
    }
  plik.close();
  plik2.close();
}

void zad2(){

  array2D array;
  double prev_a, a, tol=1e-6;
  int it=0;
  std::fstream map, iter;

  iter.open("zad2.iter",std::fstream::out);

  prepareInitialValueEx2(array);

  Neuman(array);
  calculatePhi(array);
  prev_a = integral(array);
  iter << it
       <<"\t"
       <<prev_a
       <<std::endl;
  it++;


  Neuman(array);
  calculatePhi(array);
  a=integral(array);
  iter << it
       <<"\t"
       <<a
       <<std::endl;
  it++;

  while(std::fabs(prev_a-a) > tol && it < 1e5)
    {
      Neuman(array);
      prev_a = a;
      calculatePhi(array);
      a = integral(array);
      it++;
      iter << it
                <<"\t"
                <<a
                <<std::endl;

     
    }
  iter.close();

  map.open("zad2.map",std::fstream::out);
  for(int x=0;x<array.size();x++)
    {
      for(int y=0;y<array[0].size();y++)
        {
          map << x
              <<"\t"
              << y
              <<"\t"
              <<array[x][y]
              <<std::endl;
        }
      map<<std::endl;
    }
  map.close();
}
 
