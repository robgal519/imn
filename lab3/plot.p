set term png
set out "zad1_calka.png"
set logscale x

plot "iteracje" u 1:2 w l

unset logscale x

set out "zad1.png"
set view map
set contours
set contour base
set cntrparam level 30
unset clabel
splot "zad1" u 1:2:3 w pm3d lt -1 

set out "zad2.png"
set logscale x
plot "zad2.iter" u 1:2 w l


unset logscale x
set out "zad2_map.png"
set view map
set size ratio -1
set contours
set contour base
set cntrparam levels 30
unset clabel
splot "zad2.map" u 1:2:3 w pm3d lt -1
